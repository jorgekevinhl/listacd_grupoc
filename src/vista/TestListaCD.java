/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;
import colecciones.ListaCD;
import java.util.Iterator;
/**
 *
 * @author Jorge
 */
public class TestListaCD {
    
    
    public static void main(String[] args) {
        
     ListaCD<String> nombres=new ListaCD();
     nombres.insertarInicio("alejandro");
     nombres.insertarInicio("peña");
     nombres.insertarInicio("boris");
     nombres.insertarInicio("alejandro");
    System.out.println(nombres);
    System.out.println(nombres.contieneElementRepet());
    System.out.println(nombres.remove(2)); 
    System.out.println(nombres);
      
//     for(String dato:nombres)
//            System.out.println(dato);
//     
//     
//     //forma 2 de usar iterador:
//     Iterator<String> it=nombres.iterator();
//     while(it.hasNext())
//            System.out.println(it.next().toString());
//     
//     
//     
//     ListaCD<Integer> numeros=new ListaCD();
//     int n=1000000;
//     
//     long tiempo=System.currentTimeMillis();
//     for(int i=0;i<n;i++)
//        numeros.insertarFinal(i);
//     
//     tiempo=System.currentTimeMillis()-tiempo;
//     tiempo=tiempo/1000;
//        System.out.println("Se demoro en insertando:"+tiempo+"segundos");
//     //De la manera menos menos menos óptima:
//     //Estamos simulando un vector   
//     tiempo=System.currentTimeMillis();
//     //variable mentirosa:
//     long c=0;
////      for(int i=0;i<numeros.getTamanio();i++)  
////            //System.out.println(numeros.get(i));
////            c+=numeros.get(i);
//        
//        for(int dato:numeros)
//              c+=dato;
//
//      tiempo=System.currentTimeMillis()-tiempo;
//     tiempo=tiempo/1000;
//        System.out.println("Se demoro en iterator:"+tiempo+"segundos");
    }
}
